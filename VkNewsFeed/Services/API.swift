//
//  API.swift
//  VkNewsFeed
//
//  Created by Katrin on 07.05.2021.
//

import Foundation

struct API {
    static let scheme = "https"
    static let host = "api.vk.com"
    static let version = "5.130"
    
    static let newsFeed = "/method/newsfeed.get"
    
    static let user = "/method/users.get"
}
