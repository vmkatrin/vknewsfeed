//
//  UserResponse.swift
//  VkNewsFeed
//
//  Created by Katrin on 17.05.2021.
//

import Foundation

struct UserResponseWrapd: Decodable {
    let response: [UserResponse]
}

struct UserResponse: Decodable {
    let photo100: String?
}
