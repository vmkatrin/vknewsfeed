//
//  NewsFeedCell.swift
//  VkNewsFeed
//
//  Created by Katrin on 11.05.2021.
//

import Foundation
import UIKit

protocol FeedCellViewModel {
    var iconUrlString: String { get }
    var name: String { get }
    var date: String { get }
    var text: String? { get }
    var likes: String? { get }
    var comments: String? { get }
    var shares: String? { get }
    var views: String? { get }
    var photoAttachments: [FeedCellPhotoAttachmentViewModel] { get }
    var sizes: FeedCellSizes { get }
}

protocol FeedCellSizes {
    var postLabelFrame: CGRect { get }
    var moreTextButtonFrame: CGRect { get }
    var attachmentFrame: CGRect { get }
    var bottomViewFrame: CGRect { get }
    var totalHeight: CGFloat { get }
}

protocol FeedCellPhotoAttachmentViewModel {
    var photoUrlString: String? { get }
    var width: Int { get }
    var height: Int { get }
}

class NewsFeedCell: UITableViewCell {
    
    static let reusedId = "NewsFeedCell"
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var iconImageView: WebImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var sharesLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var postImageView: WebImageView!
    @IBOutlet weak var bottomView: UIView!
    
    override func prepareForReuse() {
        iconImageView.set(imageUrl: nil)
        postImageView.set(imageUrl: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconImageView.layer.cornerRadius = iconImageView.frame.width / 2
        iconImageView.clipsToBounds = true
        
        cardView.layer.cornerRadius = 10
        cardView.clipsToBounds = true
        
        backgroundColor = .clear
        selectionStyle = .none
        
    }
    
//    func set(viewmodel: FeedCellViewModel) {
//        iconImageView.set(imageUrl: viewmodel.iconUrlString)
//        nameLabel.text = viewmodel.text
//        dateLabel.text = viewmodel.date
//        postLabel.text = viewmodel.text
//        likesLabel.text = viewmodel.likes
//        commentsLabel.text = viewmodel.comments
//        sharesLabel.text = viewmodel.shares
//        viewsLabel.text = viewmodel.views
//        
//        postLabel.frame = viewmodel.sizes.postLabelFrame
//        postImageView.frame = viewmodel.sizes.attachmentFrame
//        bottomView.frame = viewmodel.sizes.bottomViewFrame
//        
//        if let photoAttachement = viewmodel.photoAttachments {
//            postImageView.set(imageUrl: photoAttachement.photoUrlString)
//            postImageView.isHidden = false
//
//        } else {
//            postImageView.isHidden = true
//        }
//    }
}
