## Technology

**Clean Swift Architecture** 

**User Authorization / Registration** 

**JSON Decodable** 

**Asynchronous Data Fetching** 

**Auto Layout Programmatically / using XIB**

**UITableViewCells Dynamic Behavior** 

**Loading and Caching Images**

**Nested UICollectionViews**

**UICollectionView Custom Layout**

**UINavigationController**

**CAGradientLayer & Custom Shadows**
